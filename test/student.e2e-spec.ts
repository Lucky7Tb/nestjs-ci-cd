import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { PrismaService } from '../src/prisma/prisma.service';
import { AppModule } from '../src/app.module';

describe('StudentController (e2e)', () => {
  let app: INestApplication;
  let http: request.SuperTest<request.Test>;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    http = request(app.getHttpServer());
    prismaService = app.get<PrismaService>(PrismaService);
    await app.init();
  });

  afterAll(async () => {
    await app.close();
    await prismaService.student.deleteMany({});
  });

  describe('/student (GET)', () => {
    it('Should return empty array of student', () => {
      return http
        .get('/student')
        .expect(200)
        .then((response) => {
          const { body } = response;
          expect(body.statusCode).toBe(200);
          expect(body.message).toBe('Success get all student');
          expect(body.data.length).toBe(0);
        });
    });
  });

  describe('/student (POST)', () => {
    it('Should error create student because the data was not provide', () => {
      return http
        .post('/student')
        .send({})
        .expect(400)
        .then((response) => {
          const { body } = response;
          expect(body.statusCode).toBe(400);
          expect(body.error.length).toBeGreaterThan(0);
        });
    });

    it('Should success create student', () => {
      return http
        .post('/student')
        .send({
          name: 'Jhon Doe',
          address: 'Permata Kopo',
        })
        .expect(201)
        .then((response) => {
          const { body } = response;
          expect(body.statusCode).toBe(201);
          expect(body.message).toBe('Success create student');
          expect(body.data).not.toBeNull();
          expect(body.data).not.toBeUndefined();
          expect(body.data.name).toBe('Jhon Doe');
          expect(body.data.address).toBe('Permata Kopo');
        });
    });
  });

  describe('/student/:id (DELETE)', () => {
    it('Should success delete student', async () => {
      const response = await http
        .post('/student')
        .send({
          name: 'Jhon Doe',
          address: 'Permata Kopo',
        })
        .expect(201);

      return http
        .delete('/student/' + response.body.data.id)
        .expect(200)
        .then((response) => {
          const { body } = response;
          expect(body.statusCode).toBe(200);
          expect(body.message).toBe('Success delete student');
        });
    });
  });

  it('/student (GET)', () => {
    return http
      .get('/student')
      .expect(200)
      .then((response) => {
        const { body } = response;
        expect(body.statusCode).toBe(200);
        expect(body.message).toBe('Success get all student');
        expect(body.data.length).toBeGreaterThan(0);
      });
  });
});
