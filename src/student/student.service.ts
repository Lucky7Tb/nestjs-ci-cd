import { HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateStudentDto } from './dto/create-student.dto';

@Injectable()
export class StudentService {
  constructor(private readonly prismaService: PrismaService) {}

  async getAllStudent() {
    const students = await this.prismaService.student.findMany();
    return {
      statusCode: HttpStatus.OK,
      message: 'Success get all student',
      data: students,
    };
  }

  async createStudent(studentDto: CreateStudentDto) {
    const student = await this.prismaService.student.create({
      data: studentDto,
    });

    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success create student',
      data: student,
    };
  }

  async deleteStudent(id: string) {
    const student = await this.prismaService.student.delete({
      where: {
        id,
      },
    });
    return {
      statusCode: HttpStatus.OK,
      message: 'Success delete student',
      data: student,
    };
  }
}
