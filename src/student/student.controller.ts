import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { StudentService } from './student.service';

@Controller('student')
export class StudentController {
	constructor(private readonly studentService: StudentService) { }
	
	@Get()
	async getAllStudent() {
		return this.studentService.getAllStudent();
	}

	@Post()
	async createStudent(@Body() studentDto: CreateStudentDto) {
		return this.studentService.createStudent(studentDto);
	}

	@Delete('/:id')
	async deleteStudent(@Param('id', ParseUUIDPipe) id: string) {
		return this.studentService.deleteStudent(id);
	}
}
